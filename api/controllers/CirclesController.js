/**
 * CirclesController
 *
 * @description :: Server-side logic for managing circles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

async function mockCircles(){
  return new Promise(async (resolve, reject) => {
    try {
      let circlesMock = [
        {name: "A poor circle mock", payout_amount: 2000},
        {name: "A very poor circle mock", payout_amount: 1000},
        {name: "A moderate circle mock", payout_amount: 5000},
        {name: "A good circle mock", payout_amount: 8000},
        {name: "An excellent circle mock", payout_amount: 20000},
      ];
      const circle = await Circles.create(circlesMock);
      
      return resolve(circle)
    } catch (error) {
      reject(error)
    }
  })
}

async function mockCircleJoins(circles){
  return new Promise(async (resolve, reject) => {
    try {
      
      // Populating circleJoins with their related circles.
      let mockedCircleJoins = [];
      
      for(var circlesLength = 0; circlesLength < circles.length; circlesLength++){
        for(var slotNumber = 0; slotNumber < 10; slotNumber++){
          mockedCircleJoins.push({circle: circles[circlesLength].id, position: slotNumber, uuid: circlesLength})
        }
      }

      const circleJoins = await Circle_join.create(mockedCircleJoins);
      
      return resolve(circleJoins)
    } catch (error) {
      reject(error)
    }
  })
}

async function mockCirclesOccupation(occupationPercentages){
  return new Promise(async (resolve, reject) => {
    try {
      
      let usersMock = [{name: "Belal",limit: 2000}]
      const users = await User.create(usersMock);

      let circleJoins = await Circle_join.find({});

      for(var i = 0; i < occupationPercentages.length; i++){
        // result here is set of all records with a slot number that matches the position.
        let result = circleJoins.filter((obj) => obj.position === i)
        // splicing with percentage, this to ignore based on that percentage.
        result = result.splice(0, (occupationPercentages[i]/100)*result.length)
        for(var j = 0; j < result.length; j++){
          await Circle_join.update({id: result[j].id}, {user: users[0].id})
        }
      }

      return resolve(await Circle_join.find({}))
    } catch (error) {
      reject(error)
    }
  })
}

async function shouldAddCircle(payoutAmount, exact, margin=0.0001, occupationPercentages){
  return new Promise(async (resolve, reject) => {
    try {
      
      if(margin === 0){
        return resolve({ERROR: "Margin can't be 0"})
        // reject(Error("Margin can't be 0"))
      }

      // Filtering the circles based on the payout amount
      let circlesResultIds = await Circles.find({payout_amount: {'<=': [payoutAmount]}})
      circlesResultIds = circlesResultIds.map((obj) => obj.id)
      
      // getting all of the circle joins based on the list of ids of their circles, and sorting it ascending
      const result = await Circle_join.find({where: {circle: circlesResultIds}, sort: 'circle ASC'})

      // for linking each circle with its circle joins
      let correspondentCircleJoin = []
      // for aggregating the whole results in one array
      let finalResult = []

      // this is the loop that connects each circle with their correspond circle join
      for(var i = 0; i < circlesResultIds.length; i++){
        correspondentCircleJoin.push({circle: circlesResultIds[i], data: result.filter((obj) => obj.circle === circlesResultIds[i])})
      }

      // This is where the magic happens.
      for(var i = 0; i < occupationPercentages.length; i++){
        // counting how many occupied and non occupied on the scope of each circle to its circle joins
        let count = {occupied: 0, non_occupied: 0}
        for(var j = 0; j < correspondentCircleJoin.length; j++){
          for(var k = 0; k < correspondentCircleJoin[j].data.length; k++){
            // checking the number of occupation down here
            if(correspondentCircleJoin[j].data[k].position === i && correspondentCircleJoin[j].data[k].user == null){
              count.non_occupied += 1
            } else if(correspondentCircleJoin[j].data[k].position === i && correspondentCircleJoin[j].data[k].user != null) {
              count.occupied += 1
            }
          }
        }
        // counting the total positions to calculate the ratio
        let totalPositions = count.non_occupied+count.occupied
        
        let shouldAdd = exact ? (count.non_occupied / totalPositions)*100 >= occupationPercentages[i]-margin : (count.non_occupied / totalPositions)*100 <= occupationPercentages[i]-margin
        
        finalResult.push({position: i, count, totalPositions, staticPercentage: {value:occupationPercentages[i], withMargin: occupationPercentages[i] - margin}, shouldAdd})
      }

      return resolve(finalResult)
    } catch (error) {
      reject(error)
    }
  })
}

async function getCirclesForUser(id, exact=true, margin=0.0001){
  return new Promise(async (resolve, reject) => {
    try {
      
      if(margin === 0){
        return resolve({ERROR: "Margin can't be 0"})
        // reject(Error("Margin can't be 0"))
      }

      const [fetchedUser] = await User.find({id})

      // used to determine available places of joins. 
      // i.e: limit: 1000 means => last 1 place of circles of 2000
      const userLimitFirstNumber = parseInt(fetchedUser.limit.toString()[0])
      
      const circlesForUser = await Circles.find({})
      const circleJoinsForUser = await Circle_join.find({})
      
      let result = {
        availableCirclesToJoin: [],
        availableCircleJoinsToJoin: []
      }

      // A for loop to loop over circles and find the best match
      for(var i = 0; i < circlesForUser.length; i++){
        if(fetchedUser.limit + fetchedUser.limit*(margin/100) >= circlesForUser[i].payout_amount){
          result.availableCirclesToJoin.push(circlesForUser[i])
        }
        
        let correspondentCircleJoin = circleJoinsForUser.filter((obj) => obj.circle === circlesForUser[i].id)
        
        // This to get the position where the user can occupy based on the limit
        let positionFlag = correspondentCircleJoin.length - userLimitFirstNumber
        if(exact){
          for(var j = positionFlag; j < correspondentCircleJoin.length; j++){
            if(correspondentCircleJoin[j].user == null && result.availableCirclesToJoin.filter(obj => obj.id === correspondentCircleJoin[j].circle).length === 0){
              result.availableCircleJoinsToJoin.push(correspondentCircleJoin[j])
            }
          }
        } else {
          result.availableCircleJoinsToJoin.push(correspondentCircleJoin)
        }
      }

      return resolve (result);
      
    } catch (error) {
      reject(error)
    }
  })
}

// static occupation array of percentages
const occupationPercentages = [80, 80, 70, 60, 50, 30, 20, 10, 0, 0];

module.exports = {
	mock: (async function (req, res){

    const circles = await mockCircles();
    
    const circleJoin = await mockCircleJoins(circles);
    
    const occupation = await mockCirclesOccupation(occupationPercentages);

    return res.json({circleJoin})
  }),

  shouldAddCircle: (async function(req, res) {
    const {payoutAmount, exact, margin} = req.body;
    
    const shouldAddCircleResult = await shouldAddCircle(payoutAmount, exact, margin, occupationPercentages)
    
    return res.json({shouldAddCircleResult});
  }),

  getCirclesForUser: (async function(req, res) {
    const {id, exact, margin} = req.body;
    
    const circles = await getCirclesForUser(id, exact, margin)

    return res.json(circles)
  })

};

