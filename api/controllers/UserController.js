/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

async function register(name, limit){
  return new Promise(async (resolve, reject) => {
    try {
      let usersMock = {name,limit}
      const result = await User.create(usersMock);
      return resolve(result)
    } catch (error) {
      reject(error)
    }
  })
}

module.exports = {
  register: (async function(req, res) {
    const {name, limit} = req.body;
    const registeredUser = await register(name, limit)
    return res.json({registeredUser})
  })
};

