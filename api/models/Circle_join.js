/**
 * Circle_join.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'MoneyFellowsMySQL',
  tableName: 'circle_join',

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
    },
    uuid: {
      type: 'integer'
    },
    position: {
      type: 'integer'
    },
    user: {
      model: 'User',
      required: false
    },
    circle: {
      model: 'Circles',
      required: false
    }
  }
};

