# MoneyFellows-Filters

> It is recommended for using **Postman** for the requests.

**Steps for making things work:**
 
- **[REQUIRED STEP]** to populate the database with the required records:
	 - GET http://localhost:1337/api/circles/mock

- to test shouldAddCircle:
	- POST http://localhost:1337/api/circles/should-add 
  - Parameters are documented in the config/routes of that route.

- to test getCirclesForUser:
	- POST http://localhost:1337/api/circles/get-for-user
  -Parameters are documented in the config/routes of that route.